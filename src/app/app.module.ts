import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import {MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule} from '@angular/material';
import { Task1Component } from './tasks/task1/task1.component';
import { Task3Component } from './tasks/task3/task3.component';
import { Task4Component } from './tasks/task4/task4.component';
import { Task5Component } from './tasks/task5/task5.component';
import { Task6Component } from './tasks/task6/task6.component';
import { Task7Component } from './tasks/task7/task7.component';
import { Task8Component } from './tasks/task8/task8.component';
import { Task9Component } from './tasks/task9/task9.component';
import { Task10Component } from './tasks/task10/task10.component';
import { Task11Component } from './tasks/task11/task11.component';
import { TasksListComponent } from './tasks/tasks-list/tasks-list.component';
import {ParentComponent} from './tasks/task1/parent/parent.component';
import {ChildComponent} from './tasks/task1/parent/child/child.component';
import { TaskOneDescriptionComponent } from './tasks/task1/task-one-description/task-one-description.component';
import { Task4Part2Component } from './tasks/task4-part2/task4-part2.component';
import { NestedRoute1Component } from './tasks/task6/nested-route1/nested-route1.component';
import { NestedRoute2Component } from './tasks/task6/nested-route2/nested-route2.component';
import { DemoNgContentComponent } from './tasks/task8/demo-ng-content/demo-ng-content.component';
import { AutoFormatCurrencyDirective } from './tasks/task9/directives/auto-format-currency.directive';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    Task1Component,
    Task3Component,
    Task4Component,
    Task5Component,
    Task6Component,
    Task7Component,
    Task8Component,
    Task9Component,
    Task10Component,
    Task11Component,
    TasksListComponent,
    ParentComponent,
    ChildComponent,
    TaskOneDescriptionComponent,
    Task4Part2Component,
    NestedRoute1Component,
    NestedRoute2Component,
    DemoNgContentComponent,
    AutoFormatCurrencyDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
