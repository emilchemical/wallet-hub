import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-task10',
  templateUrl: './task10.component.html',
  styleUrls: ['./task10.component.scss']
})
export class Task10Component implements OnInit {

  interpolation = 'hard coded text';
  twoWayDataBinding = 'Two-way';
  localReference;
  eventTarget;

  constructor() {
  }

  ngOnInit() {
  }

  localReferenceUpdate(input) {
    this.localReference = input;
  }

  eventTargetBinding(input) {
    this.eventTarget = input;
  }
}
