import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyFeatureRoutingModule } from './lazy-feature-routing.module';
import { LazyComponent } from './lazy/lazy.component';

@NgModule({
  imports: [
    CommonModule,
    LazyFeatureRoutingModule
  ],
  declarations: [LazyComponent]
})

export class LazyFeatureModule { }
