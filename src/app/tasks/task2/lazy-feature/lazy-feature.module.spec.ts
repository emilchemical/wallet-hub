import { LazyFeatureModule } from './lazy-feature.module';

describe('LazyFeatureModule', () => {
  let lazyFeatureModule: LazyFeatureModule;

  beforeEach(() => {
    lazyFeatureModule = new LazyFeatureModule();
  });

  it('should create an instance', () => {
    expect(lazyFeatureModule).toBeTruthy();
  });
});
