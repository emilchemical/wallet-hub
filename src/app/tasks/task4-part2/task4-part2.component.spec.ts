import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Task4Part2Component } from './task4-part2.component';

describe('Task4Part2Component', () => {
  let component: Task4Part2Component;
  let fixture: ComponentFixture<Task4Part2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Task4Part2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Task4Part2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
