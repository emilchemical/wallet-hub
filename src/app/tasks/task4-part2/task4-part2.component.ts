import {Component, OnInit} from '@angular/core';
import {SharedDataService} from '../task4/services/shared-data.service';

@Component({
  selector: 'app-task4-part2',
  templateUrl: './task4-part2.component.html',
  styleUrls: ['./task4-part2.component.scss']
})
export class Task4Part2Component implements OnInit {

  sharedDataFromService;

  constructor(private sharedDataService: SharedDataService) {
  }

  ngOnInit() {
    this.sharedDataService.notify().subscribe(input => {
      // this.sharedDataFromService = input;
    });

    this.sharedDataFromService = this.sharedDataService.sharedData;
  }

}
