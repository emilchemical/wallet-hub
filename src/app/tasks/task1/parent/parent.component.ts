import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {

  childName;
  messageFromChild;

  constructor() {
  }

  ngOnInit() {
  }

  setChildName() {
    this.childName = 'I have a name';
  }

  messageFromChildEvent(data) {
    this.messageFromChild = data;
  }
}
