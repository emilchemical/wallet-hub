import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnChanges {

  @Input()
  name;

  @Output()
  change: EventEmitter<string> = new EventEmitter<string>();

  childName;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.name && !changes.name.firstChange) {

      this.childName = changes.name.currentValue;

      // Output
      this.change.emit(`My name has changed to "${this.childName}"`);
    }
  }
}
