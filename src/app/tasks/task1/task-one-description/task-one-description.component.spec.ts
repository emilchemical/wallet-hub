import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskOneDescriptionComponent } from './task-one-description.component';

describe('TaskOneDescriptionComponent', () => {
  let component: TaskOneDescriptionComponent;
  let fixture: ComponentFixture<TaskOneDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskOneDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskOneDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
