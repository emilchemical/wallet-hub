import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task7',
  templateUrl: './task7.component.html',
  styleUrls: ['./task7.component.scss']
})
export class Task7Component implements OnInit {

  validationEmailMessage;

  validatePhoneNumberMessage;

  constructor() {
  }

  ngOnInit() {
  }

  emailInput(value) {
    if (!this.validateEmail(value)){
      this.validationEmailMessage = "Email is invalid!";
    }else{
      this.validationEmailMessage = '';
    }
  }

  phoneInput(value) {
    if (!this.validatePhoneNumber(value)){
      this.validatePhoneNumberMessage = "Phone number is invalid!";
    }else{
      this.validatePhoneNumberMessage = '';
    }
  }


  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validatePhoneNumber(phone){
    const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return re.test(String(phone).toLowerCase());
  }

}
