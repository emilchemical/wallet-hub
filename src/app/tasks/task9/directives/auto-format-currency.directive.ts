import {Directive, HostListener, Input, OnChanges, SimpleChanges} from '@angular/core';
import {NgControl} from '@angular/forms';

@Directive({
  selector: '[appAutoFormatCurrency]',
})
export class AutoFormatCurrencyDirective {

  @Input()
  currency: string;

  @HostListener('input', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;

    const pattern = /[\D\s\._\-]+/g;

    let currency;
    currency = input.value.replace(pattern, '');
    currency = currency ? parseInt(currency, 10) : 0;

    const autoFormatValue = (currency === 0) ? '' : currency.toLocaleString('en-US');
    const output = autoFormatValue ? `$${autoFormatValue}` : '';
    this.ngControl.control.setValue(output);
  }

  constructor(private ngControl: NgControl) {
  }

}
