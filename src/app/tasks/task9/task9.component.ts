import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-task9',
  templateUrl: './task9.component.html',
  styleUrls: ['./task9.component.scss']
})
export class Task9Component implements OnInit {

  myGroup;

  constructor() {
  }

  ngOnInit() {
    this.myGroup = new FormGroup({
      currency: new FormControl()
    });
  }

}
