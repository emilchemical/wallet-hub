import {Component, OnInit} from '@angular/core';
import {SharedDataService} from './services/shared-data.service';

@Component({
  selector: 'app-task4',
  templateUrl: './task4.component.html',
  styleUrls: ['./task4.component.scss']
})
export class Task4Component implements OnInit {

  constructor(private sharedDataService: SharedDataService) {
  }

  ngOnInit() {
  }

  send(inputField) {
    this.sharedDataService.add(inputField.value);
  }

  getSharedData() {
    return this.sharedDataService.sharedData;
  }
}
