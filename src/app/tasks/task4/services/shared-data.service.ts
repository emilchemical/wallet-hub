import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  sharedData;

  constructor() { }

  /**
   * Send messages to subscribers
   */
  private subject = new Subject<any>();

  /**
   * Notify all subscribers about changes
   */
  notify(): Observable<any> {
    return this.subject.asObservable();
  }

  /**
   * Add item
   */
  add(data): void {
    this.sharedData = data;
    this.subject.next(data);
  }
}
