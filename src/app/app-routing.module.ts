import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {Task11Component} from './tasks/task11/task11.component';
import {Task10Component} from './tasks/task10/task10.component';
import {Task9Component} from './tasks/task9/task9.component';
import {Task8Component} from './tasks/task8/task8.component';
import {Task7Component} from './tasks/task7/task7.component';
import {Task6Component} from './tasks/task6/task6.component';
import {Task5Component} from './tasks/task5/task5.component';
import {Task4Component} from './tasks/task4/task4.component';
import {Task3Component} from './tasks/task3/task3.component';
import {Task1Component} from './tasks/task1/task1.component';
import {TasksListComponent} from './tasks/tasks-list/tasks-list.component';
import {AuthGuard} from './tasks/task3/guards/auth.guard';
import {Task4Part2Component} from './tasks/task4-part2/task4-part2.component';
import {NestedRoute1Component} from './tasks/task6/nested-route1/nested-route1.component';
import {NestedRoute2Component} from './tasks/task6/nested-route2/nested-route2.component';

const routes: Routes = [
  {path: '', redirectTo: '/tasks', pathMatch: 'full'},
  {
    path: 'tasks',
    component: TasksListComponent,
    children: [
      {
        path: '1',
        component: Task1Component
      },
      {
        path: '2',
        loadChildren: './tasks/task2/lazy-feature/lazy-feature.module#LazyFeatureModule'
      },
      {
        path: '3',
        component: Task3Component,
        canActivate: [AuthGuard]
      },
      {
        path: '4',
        component: Task4Component
      },
      {
        path: '4-part-2',
        component: Task4Part2Component
      },
      {
        path: '5',
        component: Task5Component
      },
      {
        path: '6',
        component: Task6Component,
        children: [
          {
            path: 'nested-route1',
            component: NestedRoute1Component
          },
          {
            path: 'nested-route2',
            component: NestedRoute2Component
          }
        ]
      },
      {
        path: '7',
        component: Task7Component
      },
      {
        path: '8',
        component: Task8Component
      },
      {
        path: '9',
        component: Task9Component
      },
      {
        path: '10',
        component: Task10Component
      },
      {
        path: '11',
        component: Task11Component
      },
    ]
  },
  {path: '**', component: AppComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
